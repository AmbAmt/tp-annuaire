export default class User {
    constructor(prenom, nom, age, nbRue, rue, cp, ville, mail, picture) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.nbRue = nbRue;
        this.rue = rue;
        this.cp = cp;
        this.ville = ville;
        this.mail = mail
        this.picture = picture
    }

    get fullName() {
        return this.prenom + " " + this.nom;
    }

    get adresse() {
        return this.nbRue + " " + this.rue
    }

    get adresse2() {
        return this.cp + " " + this.ville
    }
}